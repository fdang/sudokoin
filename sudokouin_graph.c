/* Sudokouin Graph by coin²
utilise le pseudocode de https://en.wikipedia.org/wiki/Sudoku_solving_algorithms

Compile with :
gcc -Wall -Wextra -fopenmp -O3 -march=native `sdl-config --cflags` -o kouin graphics.c sudokouin_graph.c `sdl-config --libs` -lm -lSDL_ttf

Test with :
./kouin "9...7...." "2...9..53" ".6..124.." "84...1.9." "5.....8.." ".31..4..." "..37..68." ".9..5.741" "47......."
./kouin "9..1....5" "..5.9.2.1" "8...4...." "....8...." "...7....." "....26..9" "2..3....6" "...2..9.." "..19.457."
or
./kouin top95.txt 2
./kouin msk_009.txt 1
./kouin top95.txt 3
./kouin top2365.txt 1
./kouin top95.txt 10
./kouin top95.txt 1
./kouin subig20.txt 1
./kouin msk_009.txt 2
./kouin top100.txt 1
// txt sudoku from http://www2.warwick.ac.uk/fac/sci/moac/people/students/peter_cock/python/sudoku/
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#include <time.h>
#include <stdlib.h>

#include <omp.h>

#include "graphics.h"

#define SIZE            9  // pourrait marcher avec d'autres tailles également
#define my_assert__(x)  for ( ; !(x) ; assert(x) ) printf("Erreur\n");  // gestion erreur avec assertion

#define WINDOW_SIZE     800
#define DELTA  (WINDOW_SIZE / SIZE)
#define OFFSET 40

typedef uint_fast16_t   t_uint;
static int i = 0;
static int frequence = 1;

t_uint *read_line(char *line)
{
	t_uint *grid = (t_uint *) malloc(SIZE * SIZE * sizeof (t_uint));
	t_uint *ptr = grid;
	t_uint i;
	for(i = 0; i < SIZE * SIZE; i ++)
	{
			if(line[i] == '.')
				*(ptr ++) = 0;
			else
				*(ptr ++) = line[i] - '0';
	}

	return grid;
}

t_uint *read_file(char *filename, t_uint idx)
{
	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	fp = fopen(filename, "r");
	my_assert__(fp != NULL);

	t_uint i = 1;
	while((read = getline(&line, &len, fp)) != -1 && i <= idx) 
	{
		i ++;
	}

	// printf("Retrieved line of length %zu :\n", read);
	// printf("%s", line);

	my_assert__(read == SIZE * SIZE + 1);
	my_assert__(read != -1);
	t_uint *grid = read_line(line);

	if (line)
	   free(line);

	return grid;
}


t_uint *read_grid(char **argv)
{
	t_uint i, j;
	t_uint *grid = (t_uint *) malloc(SIZE * SIZE * sizeof (t_uint));
	t_uint *ptr = grid;

	for(i = 1; i < SIZE + 1; i ++)
	{
		for(j = 0; j < SIZE; j ++)
		{
			if(argv[i][j] == '.')
				*(ptr ++) = 0;
			else
				*(ptr ++) = argv[i][j] - '0';
		}
		my_assert__(argv[i][j] == '\0' && "Glio c le plus bo") // si ligne mal saisie
	}

	return grid;
}

void display_grid(t_uint *grid)
{
	t_uint idx;

	for(idx = 0; idx < SIZE * SIZE; idx ++)
	{
		printf("%lu ", *grid ++);
		if ((idx + 1) % SIZE == 0)
			printf("\n");
	}
}

void print_number(t_uint idx, t_uint val, COULEUR col)
{
	POINT p;

	p.x = OFFSET / 2 + ((int) (idx) % SIZE) * DELTA + DELTA / 2;
	p.y = - OFFSET / 2 + WINDOW_SIZE - ((int) (idx) / SIZE) * DELTA + DELTA / 2;
	
	POINT p2; p2.x = p.x + 62; p2.y = p.y - 62;
	draw_fill_rectangle(p, p2, noir);

	aff_int(val, 60, p, col);
}


bool is_value_allowed(t_uint *grid, t_uint idx, t_uint val)
{
	t_uint i;
	t_uint col = idx % SIZE;
	t_uint row = idx / SIZE;

	for (i = 0; i < SIZE; i ++) 
		if (*(grid + row * SIZE + i) == val || *(grid + i * SIZE + col) == val) // ligne et colonne
			return false;

	t_uint *ptr = grid + (row - (row % 3)) * SIZE + (col - (col % 3));
	for (i = 0; i < 3; i ++) // voisins de même bloc
	{
		if (*(ptr ++) == val || *(ptr ++) == val || *(ptr) == val) 
			return false;
		ptr += SIZE - 2;
	}

	return true;
}

bool solve_sudoku(t_uint *grid, t_uint idx) // backtracking
{
	t_uint val;

	if(grid[idx] == 0)
	{
		for (val = 1; val <= SIZE; val ++) // on essaye des valeurs possibles
		{
			if (is_value_allowed(grid, idx, val)) // on vérifie si val est acceptable
			{
				grid[idx] = val;
				// COINCOIN
				// if (rand() % 1000 == 0)
				if ((i ++) % (frequence) == 0)
					print_number(idx, val, green);
				if (solve_sudoku(grid, idx + 1)) // val est accepté on procède au suivant
				{
					return true;
				}
			}
		}
	}
	else 
		return solve_sudoku(grid, idx + 1);  // case non vide on passe à l'autre

	grid[idx] = 0;

	if (idx >= SIZE * SIZE - 1)  // faut bien arrêter un moment donné...
		return true;

	return false;
}


void print_quadrillage()
{
	int i, j;
	POINT p1, p2;
	for (i = DELTA; i < DELTA * SIZE; i = i + DELTA)
	{
		p1.x = OFFSET;            		p1.y = i + OFFSET;
		p2.x = DELTA * SIZE + OFFSET;   p2.y = i + OFFSET;
		draw_line(p1, p2, blanc);
	}
	
	for (j = DELTA; j < DELTA * SIZE; j = j + DELTA)
	{
		p1.x = j + OFFSET;  p1.y = OFFSET;
		p2.x = j + OFFSET;  p2.y = DELTA * SIZE + OFFSET;
		draw_line(p1, p2, blanc);
	}
}

void display_sudoku(t_uint *grid, COULEUR color)
{
	t_uint idx;

	for(idx = 0; idx < SIZE * SIZE; idx ++)
		if (grid[idx] != 0)
			print_number(idx, grid[idx], color);
}

int main(int argc, char **argv)
{
	double start, end;

	init_graphics(WINDOW_SIZE + OFFSET, WINDOW_SIZE + OFFSET + 10);
	print_quadrillage();
	
	srand(time(NULL));

	my_assert__(argc == SIZE + 1 || argc == 3);
	t_uint *grid = NULL;

	if (argc == SIZE + 1)
		grid = read_grid(argv);     // lecture des lignes
	else if (argc == 3)
		grid = read_file(argv[1], atoi(argv[2]));

	display_sudoku(grid, blue);

	int vitesse;
	vitesse = lire_entier_clavier() + 1;
	while(-- vitesse)
		frequence *= 10;

	start = omp_get_wtime();
	my_assert__(solve_sudoku(grid, 0));
	end = omp_get_wtime();

	// display_grid(grid);
	display_sudoku(grid, violet);

	free(grid);

	if (argc == SIZE + 1)
		grid = read_grid(argv);     // lecture des lignes
	else if (argc == 3)
		grid = read_file(argv[1], atoi(argv[2]));

	display_sudoku(grid, blue);

	free(grid);

	POINT p; p.x = 2; p.y = 26;
	aff_pol("by coin \\_o<", 16, p, gris);

	wait_escape(end - start);

	return EXIT_SUCCESS;
}
